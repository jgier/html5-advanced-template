$(document).ready(function() {

//jquery goes here

//prevents standalone mode(iOS) links from opening iOS safari, unless target="_blank"
	$(document).on('touchend click', 'a', function(e) {

	    if ($(this).attr('target') !== '_blank') {
	        e.preventDefault();
	        window.location = $(this).attr('href');
	    }

	});
	
});